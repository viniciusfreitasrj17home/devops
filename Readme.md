# Initial Configuration

## For Development
## OBS: Access in Minikube VM: username: "docker", password: "tcuser".
## Run Step 1:

### minikube delete; // If what many others projects
### minikube config set driver virtualbox
### minikube start --memory='3300mb' --cpus=2 --disk-size='25000mb'

### add minikube ip in /etc/hosts with name: e-commerce.com

## Run Step 2:

### cd config-kubernetes

### kubectl apply -f namespaces.yaml

### kubectl apply -f config-map.yaml

### kubectl apply -f secrets.yaml

### cd rollbacks-helm

### If not have Helm, follow below to install:
cd ~/Downloads
wget https://get.helm.sh/helm-v3.4.1-linux-amd64.tar.gz
tar -zxvf helm-v3.4.1-linux-amd64.tar.gz
sudo mv linux-amd64/helm /usr/local/bin/helm
helm repo add stable https://charts.helm.sh/stable
helm repo update

### helm install --generate-name --namespace devops -f chartmuseum-conf.yaml stable/chartmuseum

### helm repo add e-commerce http://$(kubectl get nodes --namespace devops -o jsonpath="{.items[0].status.addresses[0].address}"):30333

### helm plugin install https://github.com/chartmuseum/helm-push

### helm push charts/backend-api e-commerce

### helm push charts/frontend e-commerce

### helm repo update

### helm install e-commerce/frontend --name-template frontend --namespace development

### helm install e-commerce/backend-api --name-template backend-api --namespace development

### helm ls

### cd pipelines-jenkins

### kubectl apply -f pv-pvc.yaml

### mkdir mnt
### minikube mount ./mnt:/mnt/data-jenkins --uid docker --gid docker --9p-version='9p2000.L' &

### helm repo add jenkins https://charts.jenkins.io
### helm repo update

### helm install --name-template jenkins --set persistence.existingClaim=jenkins --set controller.serviceType=NodePort --set controller.nodePort=30444 --namespace devops jenkins/jenkins

### kubectl create rolebinding sa-devops-role-clusteradmin --clusterrole=cluster-admin --serviceaccount=devops:default --namespace=devops

### kubectl create rolebinding sa-devops-role-clusteradmin-kubesystem --clusterrole=cluster-admin --serviceaccount=devops:default --namespace=kube-system

### Create Credencial do Gitlab and DockerHub

### Access Jenkins:
admin
printf $(kubectl get secret --namespace devops jenkins -o jsonpath="{.data.jenkins-admin-password}" | base64 --decode);echo